#!/usr/bin/env tdaq_python

from __future__ import print_function
import sys
import time

from ispy import IPCPartition, Merger

update = 5

if len(sys.argv) < 3:
    print("usage: ispymerge.py <partition> <config_file> [<update>]")
    print("   where each line in the input file should have:")
    print("   <servers-regex> <input-regex> <type> <publish> [ <averaged-attributes>+ ]") 
    sys.exit(1)

part_name = sys.argv[1]
conf_file = sys.argv[2]

if len(sys.argv) > 3:
    update = int(sys.argv[2])

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)

mergers = []

for line in open(conf_file).readlines():
    line = line.strip()
    if len(line) == 0: continue    
    if line.startswith('#'): continue
    (servers, input, type_name, publish, rest) = line.split(None,4)
    mergers.append(Merger(p, servers, input, type_name, rest.split(), publish))

while True:
    if p.isValid(): 
        for m in mergers:
            m.update()
        time.sleep(update)
    else:
        time.sleep(60)

    
