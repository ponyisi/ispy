#!/usr/bin/env tdaq_python

"""
Dump IS information into XML format.
"""
from __future__ import print_function

import sys
import getopt

from ipc import IPCPartition
from ispy import *

def usage():
    print("Usage: ",sys.argv[0],"-p <partition> -n <server> [-t <type> ] <regexp1> <regexp2>...")

opts = getopt.getopt(sys.argv[1:],"p:n:t:h")

partition_name = None
server_name = None
type_name = None

for opt,val in opts[0]:
    if opt == '-p':
        partition_name = val
    elif opt == '-n':
        server_name = val
    elif opt == '-t':
        type_name = val
    elif opt == '-h':
        usage()
        sys.exit(0)

if not partition_name or not server_name:
    usage()
    sys.exit(1)

expr = opts[1]
if len(expr) == 0:
   expr = [ '.*' ]

try:
    partition = IPCPartition(partition_name)

    print('<?xml version="1.0" encoding="UTF-8"?>')
    print('<objects>')
    for rex in expr:
        it = ISInfoIterator(partition, server_name, ISCriteria(rex))
        while it.next():
            if not type_name or (it.type().name() == type_name):
                obj = ISObject(partition,it.name(), it.type().name())
                obj.checkout()
                print(obj.toXML())
    print('</objects>')
except:
    print("Exception caught")
