from __future__ import division

from ispy import ISInfoIterator, ISCriteria, ISObject, ISServerIterator, Basic
import re

class Merger(object):
    """
    Sum or average all entries of a given type from a list of IS servers.
    Re-publish the result as the same type into another IS Server.
    """

    def __init__(self, partition,
                 servers, criteria, type_name, averages,  # input
                 publish):                                # output
        self.partition = partition
        self.criteria  = ISCriteria(criteria)
        self.type_name = type_name
        self.averages  = averages
        self.publish   = publish
        
        self.servers   = []        
        self.objects   = {}
        self.result    = None

        srvs = ISServerIterator(self.partition)
        while srvs.next():
            if re.match(servers, srvs.name()):
                self.servers.append(srvs.name())

    def update(self):
        self.objects = {}
        
        for srv in self.servers:
            try:
                it = ISInfoIterator(self.partition, srv, self.criteria)
                while it.next():
                    if it.type().name() == self.type_name:
                        # both name (ISCriteria) and type name match
                        x = ISObject(self.partition, it.name(), self.type_name)
                        x.checkout()
                        self.objects[x.name()] = x
                    
            except RuntimeError:
                pass
        
        # ok, we got all objects, now sum the attributes

        # first, create the result object, initialized to zero...
        self.result  = ISObject(self.partition, self.publish, self.type_name)

        doc = self.result.doc()
        for x in list(self.objects.values()):
            for i in range(0, doc.attributeCount()):
                a = doc.attribute(i)
                # ignore arrays and anything not numerical
                if not a.isArray() and a.typeCode() in [ Basic.U8, Basic.S8, Basic.U16, Basic.S16, Basic.U32, Basic.S32,
                                                         Basic.Float, Basic.Double ]:
                    value = getattr(self.result, a.name())
                    setattr(self.result, a.name(), value + getattr(x , a.name()))
                    
        # ok, now calculate the average for those attributes specified
        if len(self.objects) > 0 and len(self.averages) > 0:
            for a in self.averages:
                setattr(self.result, a, getattr(self.result, a)/len(self.objects))

        self.result.checkin(False)
