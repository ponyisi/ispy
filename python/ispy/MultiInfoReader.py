
from ispy import ISInfoIterator, ISInfoDynAny, ISCriteria, ISServerIterator

import re

class MultiInfoReader(object):
    """
    A helper class to read a subset of IS information
    and create the corresponding python objects.
    After an update() call the 'objects' attribute
    will contain all ISInfo objects that matched
    the regular expression passed in the constructor
    (defaul is '.*', i.e. all objects).

    This version can handle multipe IS Servers in the
    form of a list, where each item can in turn be a
    regular expression.
    """

    def __init__(self, partition, servers = [ '.*' ], selection = '.*'):
        self.partition = partition
        self.criteria  = ISCriteria(selection)
        self.server_criteria = servers
        self.servers    = []
        self.objects = {}
        
        self.update_servers()

    def update_servers(self):
        self.servers = []
        s = ISServerIterator(self.partition)
        while s.next():
            name = s.name()
            for r in self.server_criteria:
                if re.match(r, name):
                    self.servers.append(name)
                    break
        
    def update(self):
        "Update objects from IS server"
        self.objects = {}
        for s in self.servers:

            try:
                it = ISInfoIterator(self.partition, s, self.criteria)

                while it.next():
                    any = ISInfoDynAny()            
                    it.value(any)
                    self.objects[it.name()] = any
            except:
                pass
