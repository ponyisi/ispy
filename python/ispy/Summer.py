from __future__ import print_function
from __future__ import division

from ispy import MultiInfoReader

class Summer(MultiInfoReader):
    """
    A mixin class that can be used to sum the named
    attributes from a MultiInfoReader.
    """
    
    def __init__(self, prefix, partition, servers, criteria, attributes, averages = []):
        MultiInfoReader.__init__(self, partition, servers, criteria)
        self.prefix = prefix
        self.attributes = attributes
        self.averages   = averages
        for attr in self.attributes:
            setattr(self, attr, 0)
        for avg in self.averages:
            setattr(self, avg, 0)

    def update(self):
        """
        Update all values, then sum and average attributes.
        """
        MultiInfoReader.update(self)
        for attr in self.attributes:
            setattr(self, attr, 0)
            for i in self.objects:
                setattr(self, attr, getattr(self, attr) + getattr(self.objects[i], attr))
                
        for avg in self.averages:
            setattr(self, avg, 0)
            for i in self.objects:
                setattr(self, avg, getattr(self, avg) + getattr(self.objects[i], avg))
            if self.objects:
                setattr(self, avg, getattr(self, avg)/len(self.objects))

    def dump(self):
        """
        Print all sums and averages to screen.
        """
        for attr in self.attributes + self.averages:
            print(self.prefix + '.' + attr,':', getattr(self, attr))

            
