
def _init_environ():
    import sys
    import os
    sys.path.append(os.environ['ROOTSYS'] + '/lib')

_init_environ()    

import ROOT
import ipc

from libohpy import *

# Default implementation of 'receive' for OHRootReceiver.
# Override in user code if required.
def _receive_(self, obj):
    pass

OHRootReceiver.receive = _receive_

import ispy

OHServerIterator = ispy.ISServerIterator
OHInfoIterator   = ispy.ISInfoIterator

