#!/usr/bin/env tdaq_python

# Configuration

from __future__ import print_function

part_name = 'rhauser_test'

# End Configuration

import sys
if len(sys.argv) > 1:
    part_name = sys.argv[1]
import time

from ispy import *
from ispy.InfoReader import *

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)

####################################################

reader = InfoReader(p, 'DF')
reader.update()
print(reader.objects)

while True:
    reader.update()
    try:
        x = reader.objects['DF.L2PU-8191']
        print(x)
    except KeyError:
        print("No such object", 'DF.L2PU-8191')
    time.sleep(5)


####################################################


def show_servers():

    srvit = ISServerIterator(p)

    while srvit.next():
        print("=============================")
        print("Name:",srvit.name())    
        print("Entries:",srvit.entries())
        print("Host:",srvit.host())
        print("Owner:",srvit.owner())
        print("PID:",srvit.pid())
        print("=============================")

#####################################################
