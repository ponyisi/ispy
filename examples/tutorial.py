#!/usr/bin/env tdaq_python

"""
A tutorial showing how to use all the classes in the ispy python package.

Scripts written to use this package should typically start
with a

#!/usr/bin/env tdaq_python

to pick up the correct interpreter.
"""
from __future__ import print_function

#
# The 'ipc' package exports one class and one
# one function. We always need this but the
# 'IPCPartition' class is also available through
# the 'ispy' package for backward compatiblity.
#
from ipc import IPCPartition, getPartitions

# To make our life easier we import everything from
# the 'ispy' package
from ispy import *

# Typically we'll need a few other packages as well
# depending on what we are doing.
import sys

# We start with initializing a partition object. We
# either use a partition provided on the command line
# or present a list of available partitions which
# we get from the 'ipc' package.

if len(sys.argv) > 1:
    p = IPCPartition(sys.argv[1])
else:
    print("You must provide a partition name as parameter.")
    print("The available partitions are:")
    print()
    for partition in getPartitions():
        print(partition.name())
    sys.exit(1)

# The partition has two useful methods: one to get
# its name and one to check if it is valid.

print("You have chosen partition:", p.name())
if not p.isValid():
    print("However, this is not a valid partition.")
    print("Exiting...")
    sys.exit(1)
    
    
# Once we have the partition we can query all
# kind of useful information. E.g. the list of IS servers:

svr_it = ISServerIterator(p)

# As usual you can call dir(svr_it) or help(srv_it) on
# all objects to get an idea about the available methods.

print("There are ",svr_it.entries(), " IS servers in this partition.")

# We have two ways to iterate over the list of servers: one modelled
# after the original C++ class:

print("Their names are:")

while svr_it.next():
    # These methods are only valid after svr_it.next() has been
    # called and returns true
    print(svr_it.name(), svr_it.host(), svr_it.owner(),svr_it.time(), svr_it.pid())


# The class also implements the Python iterator interface, where
# a 5-tuple is returned consisting of the above information:

print("Their names are again:")

for s in svr_it:
    print(s)

# You extract the information you need from these:

server_names = [ s[0] for s in ISServerIterator(p) ]
print(server_names)

#
# To find one or more objects one can use the ISInfoIterator class.
# It takes an IS server name and an ISCriteria object to select a subset
# of objects in the server. Note that it only retrieves the name, tag and
# time of the object, not the value itself.
#
#
it = ISInfoIterator(p, 'RunCtrl', ISCriteria('.*'))

#
# The ISCriteria constructor takes either a string (a regular expression)
# or an ISType object (see later) or both a string and an ISType.
#

# Like the ISServerIterator there is an interface modelled after the
# C++ original or a pythonic interface using the iterator protocol:
#

print("All objects in the RunCtrl server:")
while it.next():
    print(it.name(), it.type().name(), it.time(), it.tag())

print()
print("And again:")
print()

# This returns a tuple with the values we retrieved by hand
# above:
for obj in it:
    print(obj)

# and again...
runctrl_object_names = [ obj[0] for obj in ISInfoIterator(p, 'RunCtrl', ISCriteria('.*')) ]
print(runctrl_object_names)

#
# To read IS objects, one should use an instance of ISInfoDynAny or one of its subclasses.
# To access them, an ISInfoDictionary can be used:
#
d = ISInfoDictionary(p)
val = ISInfoDynAny()
d.getValue('RunCtrl.RootController', val)
print(val)

# If you just wants to retrieve the object, you can
# also use the container protocol:
val = d['RunCtrl.RootController']

# Publishing happens by assigning. Note that this does
# not allow you to specify the history flags or tags (see below).
#
# d['RunCtrl.RootController'] = val

# The dictionary has a number of useful methods:

print('RootController exists ? ', d.contains('RunCtrl.RootController'))

# just in case something is left over...
if d.contains('RunCtrl.SomeOtherName'):
    d.remove('RunCtrl.SomeOtherName')
    
# One can insert new values
d.insert('RunCtrl.SomeOtherName', val)

# or update them if they exist
d.update('RunCtrl.SomeOtherName', val)

# or remove them.
d.remove('RunCtrl.SomeOtherName')

# checkin() can be used instead of insert/update depending on
# the desired usage:
d.checkin('RunCtrl.SomeOtherName', val)
d.remove('RunCtrl.SomeOtherName')

# The update() and checkin() method take an additional boolean
# flag if the history should be kept (default is False):

d.checkin('RunCtrl.SomeOtherName', val, True)
d.checkin('RunCtrl.SomeOtherName', val, True)
d.checkin('RunCtrl.SomeOtherName', val, True)

# We now have 3 entries with the same name in the IS server.

# We can also specify an explicit alternate tag for checkin() or update()
d.checkin('RunCtrl.SomeOtherName', 10, val)

# which adds yet another object. We can retrieve those objects
# with an additional parameter to getValue():

val2 = ISInfoDynAny()
d.getValue('RunCtrl.SomeOtherName', 10, val2)
print(val2.tag())

# One can access the whole history or all tags with the
# getValues() method efficiently:

#values = ISInfoDynAnyVector(p,'RCStateInfo')
#d.getValues('RunCtrl.SomeOtherName', values, -1)
#for v in values:
#    print v.tag()
#    print v

# You can find all tags of a given object either
# via the tags() method of the ISInfoIterator and by
# calling the getTags() method of the dictionary.
# (Note: this requires a round-trip to the IS server !)
print("Tags: ", d.getTags('RunCtrl.SomeOtherName'))

# clean up again
d.remove('RunCtrl.SomeOtherName')

#
# Once we have an ISInfoDynAny object of a given type, it behaves
# as if it has all the attributes of the IS object. We can access
# these attributes in multiple ways. Some ways do not require access
# to the underlying meta data (i.e. the names of the attributes) but
# the most convenient ones do.
#

# We can simply print all attribute values without names.
# No meta-data is required for this.
for a in val:
    print(a)

# You can also just access the n'th attribute with:

print(val[0])

# This assumes that you know the order of the attributes and
# what each one means. In most cases you don't care about the
# order but you know the name of the attribute you are interested
# in. e.g. the 'state' of the RootController object.
#
# You can access these attributes either via the container interface
# i.e. the object behaves like a dictionary:

print("State of RootController:", val['state'])

# or directly by accessing it like an attribute:
print("State of RootController:", val.state)

#
# While the latter looks more natural, it is sometimes not
# possible to use: e.g. if the IS attribute name conflicts with
# another method name of the ISInfoDynAny class, e.g. time(), type(),
# or if the attribute name is not a valid Python identifier, e.g.
# because it contains white space. 
#

# We can also modify the objects in the same way:
val.state = 'CONNECTED'

# Note that type errors are caught by the underlying
# C++ binding:

try:
    val.state = 3.4
except Exception as ex:
    print("Nope, got exception:",ex)

#
# Instead of operating with an ISInfoDictionary and ISInfoDynAny
# we can use the equivalent of ISNamedInfo of C++ in the form
# of the ISObject class.
#

# Note that we have to specify the type here
obj = ISObject(p,'RunCtrl.RootController', 'RCStateInfo')

if obj.exists():
    obj.checkout()
    print(obj)
else:
    print(obj.name()," does not exist")

# The checkin() method supports the usual
# boolean flag to indicate to keep the history or
# the use of an explicit tag.

obj.checkin(True)
obj.checkin(5)

#
# while checkout() allows to checkout a given tag
obj.checkout(5)


# A list of tags is also available (note: round-trip
# to server !). Tags are simply a tuple consisting
# of the integer tag and the time.
print("Tags: ",obj.tags())

#
# ISObject is a subclass of ISInfoDynAny and behaves
# the same otherwise.
#

# To make  the use of the ISObject class more natural
# one can also use a magic module that allows you
# write the above in the following form:
# 'IS' is a module imported from 'ispy'.
#
# Note that you can *not* say:
#   from IS import *
#

obj = IS.RCStateInfo(p, 'RunCtrl.RootController')
try:
    # Your partition may not have a HLT Supervisor
    hltsv = IS.HLTSV(p, 'DF.HLTSV')
    print("Types equal ?",type(hltsv) == type(obj))
except:
    pass

# The list of attributes can be determined dynamically
# by either calling the underlying C++ methods:

print("Object has ",obj.getAttributesNumber(),"attributes")

for i in range(obj.getAttributesNumber()):
    print(obj[i])

# or in a more pythonic way by:

for attr_name in list(obj.keys()):
    print(attr_name,obj[attr_name])

# Nested objects are fully supported (as they are needed for histograms
# or DQM Result objects e.g.). However, their use is a bit restricted to
# make sure that the user never publishes an invalid type.

dqm = IS.Result(p, 'DQM.Example')
dqm.status = 1
dqm.objects = [ 'a', 'b', 'c' ]

# We cannot assign to the .tags array directly, we can only change
# its size and then modify the existing objects (of type Tag):

# Here we have to use the alternate 'dict style' access since there is a method
# called tags() on the ISObject:

dqm['tags'].resize(2)
dqm['tags'][0].name = 'SomeName'
dqm['tags'][0].value = 3.141

dqm.checkin()

# There is a convenience method .set() in ISObject that allows you
# to copy an existing dir -like object and use an arbitrary number
# of keyword arguments in addition:
dqm['tags'][0].set({ 'name' : 'SomeOtherValue', 'value': 5.67 })
# or
dqm['tags'][1].set(name='AString', value=1.234)
dqm.checkin()

print(dqm)
for t in dqm['tags']:
    print(t)

# Note that ISObjects also behave appropriately, so if you have
# another object which you want to copy and/or copy and modify
# a few values, you can say:
#
#  dqm['tags'][0].set(other_TagObject)
#
# which copies all attributes, or:
#
#  dqm['tags'][0].set(other_TagObject, value=10.0)
#
# which first copies all attributes than overrides the 'value'
#

#
# For efficient access to many objects use the ISInfoStream class
#
# Note how we use ISCriteria with an ISType object by
# creating a dummy object and asking for its type().
#
s = ISInfoStream(p, 'RunCtrl', ISCriteria(IS.RCStateInfo(p, '').type()), False, 1)

# As usual there is an interface modeled after the C++ version and
# a python version:
while not s.eof():
    print(s.name(), s.type().name())
    x = ISInfoDynAny()
    # get() advances the stream
    s.get(x)
    print(x)

# with iterator protocol:
#
s = ISInfoStream(p, 'RunCtrl', ISCriteria(IS.RCStateInfo(p, '').type()), False, 1)
for x in s:
    print(x)

# For subscription based notifications you need an ISInfoReceiver:

recv = ISInfoReceiver(p)

# Now you can subscribe and ask to be informed either with
# an ISCallbackEvent or an ISCallbackInfo
#
# callback.reason() is of type 'Reason' (CREATE, UPDATE, DELETE)
#

def myhandler(cb):
    print(cb.name(), cb.time(), cb.type().name(),cb.reason())
    # value = ISInfoDynAny()
    # cb.value(value)
    # print value

# recv.subscribe_event('DF.DFM-1', myhandler)
recv.subscribe_info('DF.HLTSV', myhandler)

# subscribe to all RCStateInfo changes
recv.subscribe_info('RunCtrl', myhandler, ISCriteria(IS.RCStateInfo(p,'').type()))

import time

try:
    print("Press Ctrl-C to continue")
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    pass
finally:
    recv.unsubscribe('DF.HLTSV', True)
    recv.unsubscribe('RunCtrl', ISCriteria(IS.RCStateInfo(p,'').type()), True)

#
# There are other classes to access the meta-data for a given
# object, most are only useful for general purpose tools like
# browsers etc.

#
# ISInfoDocument contains information about a class.
#   infoDoc = obj.getDescription()
# ISInfoAttribute contains information about a single attribute.
#

###########################################################################################
#
# OH tutorial
#
###########################################################################################
#
# The 'oh' package gives us access to histograms
# and returns directly ROOT.TH* objects. Don't
# import this if you don't use it since it also brings
# in ROOT.
#
import oh

# The most common case is to retrieve a histogram from OH.
#
# Note that the Python bindings to not require you to know
# the type of the histogram in advance. It will return in
# all cases the appropriate ROOT object at run-time
# (TH*, TProfile, TGraph*)

try:
    h = oh.getRootObject(p, 'Histogramming','HLTSV', 'ProcessingTime')
    # h.Draw()

    # A variant also returns the time, tag  and annotations of the histogram:
    (h,h_time,h_tag,h_annotations) = oh.getRootObjectInfo(p, 'Histogramming','HLTSV', 'ProcessingTime')
    print("Time = ",h_time)
    print("Tag =  ",h_tag)
    for a in h_annotations:
        print("Annotation = ",a)
except:
    print("oops, you don't have a HLT supervisor")
    
#
# An additional parameter can specify a specific tag:
#
# h = oh.getRootObject(p, 'Histogramming','HLTSV', '/DEBUG/L2SVHistograms/TotalL2PULatencyAcceptedEvent', 5)
