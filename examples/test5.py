#!/usr/bin/env tdaq_python

# Configuration

from __future__ import print_function

part_name = 'rhauser_test'

# End Configuration

import sys
if len(sys.argv) > 1:
    part_name = sys.argv[1]
import time

from ispy import *
from ispy.MultiInfoReader import *

p = IPCPartition(part_name)

if not p.isValid():
    print("Partition",part_name,"is not valid")
    sys.exit(1)

####################################################

reader = MultiInfoReader(p, ['DF.*', 'Monitoring.*' ])
reader.update()
print(list(reader.objects.keys()))

for i in reader.objects:
    print(reader.objects[i])
    
