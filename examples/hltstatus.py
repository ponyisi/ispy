#!/usr/bin/env tdaq_python
from __future__ import print_function
import sys
import time
import re

from ispy import InfoReader, IPCPartition

partition = IPCPartition(sys.argv[1])

reader = InfoReader(partition, 'RunCtrl', 'Supervision.*')

while True:
    reader.update()
    for i in [ obj for obj in list(reader.objects.values()) if re.match('RunCtrl.Supervision.(L2PU-[0-9]+|L2SV-[0-9]+|PT-[0-9]+$|EFD-.*|SFI-.*|SFO-.*)', obj.get_name()) ]:
        if i.membership == 'OUT':
            print(i.applicationName, i.status)
            # print i.errorDescription

    print()
    time.sleep(5)
