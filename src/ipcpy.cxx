
#include "ipc/exceptions.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "owl/time.h"

#include <boost/python.hpp>

#include <string>

using namespace boost::python;

namespace {

    void init_ipc(list py_options)
    {
        if(!IPCCore::isInitialised()) {
            std::list<std::pair<std::string, std::string>>  options;
            for(ssize_t i = 0; i < len(py_options); i++) {
                object o = py_options[i];
                std::string key = extract<std::string>(o[0]);
                std::string value = extract<std::string>(o[1]);
                options.emplace_back(key, value);
            }
            IPCCore::init(options);
        }
    }

    struct IPCInitializer {
	IPCInitializer()
	{
            setenv("TDAQ_IPC_CLIENT_INTERCEPTORS", "", 0);
	    if(!IPCCore::isInitialised() && getenv("TDAQ_ISPY_NOINIT") == nullptr) {
                std::list<std::pair<std::string, std::string>>  options;
		IPCCore::init(options);
	    }
            PyEval_InitThreads(); 
	}
    };

    list get_partitions()
    {
        std::list<IPCPartition> output;
        IPCPartition::getPartitions(output);
        list result;
        for(std::list<IPCPartition>::iterator it = output.begin();
            it != output.end();
            ++it) {
            result.append(*it);
        }
        return result;
    }

}

BOOST_PYTHON_MODULE(libipcpy)
{

    static IPCInitializer ipc_initialize;

    class_<OWLDate>("OWLDate")
        .def(init<const char*>())
        .def(init<time_t>())
	.def("year", &OWLDate::year)
	.def("month", &OWLDate::month)
	.def("day", &OWLDate::day)
	.def("str", &OWLDate::str)
	.def("c_time", &OWLDate::c_time)
	;

    class_<OWLTime, bases<OWLDate> >("OWLTime")
        .def(init<const char*>())
        .def(init<time_t>())
	.def("hour", &OWLTime::hour)
	.def("min", &OWLTime::min)
	.def("sec", &OWLTime::sec)
	.def("mksec", &OWLTime::mksec)
	.def("str", &OWLTime::str)
	.def("c_time", &OWLTime::c_time)
	;


    class_<IPCPartition>("IPCPartition")
	.def(init<std::string>())
	.def("name", &IPCPartition::name,return_value_policy<copy_const_reference>())
	.def("isValid", &IPCPartition::isValid)
	;

    class_<IPCCore>("IPCCore")
        .def("init", &init_ipc)
        .def("isInitialised", &IPCCore::isInitialised)
        .def("shutdown", &IPCCore::shutdown)
        ;

    def("getPartitions", get_partitions);

}
